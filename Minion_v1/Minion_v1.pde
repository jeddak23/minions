int WIDTH=540;
int HEIGHT=960;
float CENTER_X=WIDTH/2;
float CENTER_y=HEIGHT/2;
float START_X=CENTER_X-((WIDTH/2)/2);
float START_Y=CENTER_y-((HEIGHT/4)/2);
float POSITION=0.1;
void setup() {
  size(540, 960);
  frameRate(24);
}

void draw() {
  background(255);
  fill(random(0,255),random(0,255),random(0,255));
  for(int i=0;i<1000;i++){
    float x=random(1,WIDTH-15);
    float y=random(1,HEIGHT-15);
    float w=random(1,15);
    float h=random(1,15);
    rect(x,y,w,h);
    
  }
  fill(255,255,0);
  //body
  rect(START_X,START_Y, WIDTH/2,HEIGHT/2, 50, 50, 0, 0);
  fill(0);
  //left and right eyeglass edge
  rect(START_X,START_Y+140,50,20);
  rect(START_X+220,START_Y+140,50,20);
  fill(192,192,192);
  //left and right eyeglass frame
  circle(START_X+80,START_Y+150,110);
  circle(CENTER_X+50,START_Y+150,110);
  fill(255);
  //wight eyes
  circle(START_X+80,START_Y+150,90);
  circle(CENTER_X+50,START_Y+150,90);
  //rolling eyes
  circ(30, 0.01,START_X+80,START_Y+150,0);
  circ(30, 0.01,CENTER_X+50,START_Y+150,0);
  noFill();
  strokeWeight(10);
  arc(WIDTH/2, 605, 100, 50, 0, HALF_PI);
  strokeWeight(1);

  
}
void circ( float r, float step,float x,float y,float f)
{
  fill(f);

  float x1 = x +  r*cos(POSITION);
  float y1 = y +  r*sin(POSITION);

  ellipse(x1, y1, 25, 25);
  POSITION+=step;
}
